import json
from mwclient import Site
import requests
import yaml


def get_image_data(config: dict) -> dict:
    base_url = config["k8s"]["url"]
    response = requests.get(
        f"{base_url}/api/v1/namespaces/tf-public/configmaps/image-config",
        headers={
            "User-Agent": (
                "tf-image-bot (tools.tf-image-bot@toolforge.org) "
                + f"python-requests/{requests.__version__}"
            )
        },
        verify=config["k8s"]["verify"],
        cert=(config["k8s"]["cert"], config["k8s"]["key"]),
    )
    response.raise_for_status()
    return yaml.safe_load(response.json()["data"]["images-v1.yaml"])


def save_data_to_wikitech(data: dict, config: dict):
    page_name = config["wikitech"].get("page", "Module:Toolforge images/data.json")
    site = Site(config["wikitech"]["site"], **config["wikitech"]["mwclient"])
    page = site.pages[page_name]

    data_json = json.dumps(data)
    if data_json == page.text:
        return

    page.edit(
        data_json,
        config["wikitech"].get("summary", "Bot: Updating available container images"),
    )


def main():
    with open("config.yaml", "r") as f:
        config = yaml.safe_load(f)
    
    data = get_image_data(config)
    save_data_to_wikitech(data, config)


if __name__ == "__main__":
    main()
